#include "idt.h"
#include "../kernel/util.h"


void set_idt_gate(int n, u32 handler) 
{
    idt[n].low_offset = low_16(handler);
    idt[n].sel = KERNEL_CS;
    idt[n].always0 = 0;
    idt[n].flags = 0x8E; 
    idt[n].high_offset = high_16(handler);
}

void set_idt()
{
    idt_reg.base = (u32) &idt;
    idt_reg.limit = IDT_ENTRIES * sizeof(idt_gate_t) - 1;
    
    // Load the interrupt descriptor table register using assembly.
    
    __asm__("lidt %0" : : "m" (idt_reg));

    // Notes:        
    // Alt. way     :  __asm__ __volatile__("lidtl (%0)" : : "r" (&idt_reg));
    // lidt        :  Load interrupt descriptor table register. Put 'l' at the end
    //                to force the assembler to assume long (32-bit). Put q for 64-bit.
    //                Put nothing to let assembler deduce the size.
    // __volatile__ : means gcc should disable certain optimizations, 
    //                needed in order for certain actions to work.
    //                Note that volatile is implicit when ther are no output operands.
    // %0           : pick the first operand in the list of operands. 
    //                For example if there are two output operands and three inputs,
    //                use ‘%2’ in the template to refer to the first input operand, 
    //                ‘%3’ for the second, and ‘%4’ for the third.
    // r            : Input operand that reserves a register for &idt_reg 
    //                and assigns it to the placeholder %0.
    // m            : Memory operand is allowed. Used as a constraint to avoid problems
    //                with passing memory addresses through a register. Passing an address
    //                through a register requires a memory clobber or similar mechanism
    //                to ensure the data at that address is available in memory before
    //                the inline assembly is emitted.
}