#ifndef TYPES_H
#define TYPES_H


// Types defined to make C data types easier to work with.
typedef unsigned int 	u32;
typedef			 int 	s32;
typedef unsigned short 	u16;
typedef 	     short  s16;
typedef unsigned char   u8;
typedef          char   s8;

// Used to get the lower 16 bits of an address.
#define low_16(address) (u16)((address) & 0xFFFF)

// Used to get the high 16 bits of an address. 
// Move the bits 16 steps to the right, essentially
// removing the buttom and replacing them with the 
// upper 16.
// #define high_16(address) (u16)(((address) >> 16) & 0xFFFF) // original
#define high_16(address) low_16((address >> 16)) // see if this works.

#endif