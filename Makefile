#################################
#  - Makefile for the os-image	#
################################

# Automatically generate lists of sources using wildcards.
C_SOURCES = $(wildcard kernel/*.c drivers/*.c cpu/*.c)
HEADERS = $(wildcard kernel/*.h drivers/*.h cpu/*.h)

# Convert the *.c filenames to *.o to give a list of object files to build
OBJ = ${C_SOURCES:.c=.o cpu/interrupt.o}

# Symbols for gcc
CFLAGS = -g 
#-ggdb

#######################
#	 - General   	  #
#######################

# Create the disk image.
all: os-image

# Run the disk image.
run: os-image
	qemu-system-i386 -fda $<

# Clear away all generated files
clean: 
	rm -f *.bin *.o *.dis os-image *.elf
	rm -f kernel/*.bin kernel/*.o kernel/*.dis kernel/*.elf
	rm -f drivers/*.bin drivers/*.o drivers/*.dis kernel/*.elf
	rm -f boot/*.bin boot/*.o boot/*.diss kernel/*.elf
	rm -f cpu/*.bin cpu/*.o cpu/*.diss cpu/*.elf

######################
#	- OS-image		 #
######################

# Build the disk image that the computer loads,
# which is the combination of the compiled bootsector and kernel.
os-image: boot/boot_sector.bin kernel/kernel.bin
	cat $^ > $@

# Rule to disassemble kernel. For debugging.
os-image.dis: os-image
	ndisasm -b 32 $< > $@

######################
#	- Kernel		 #
######################

# Build the kernel binary from two object files
#	- the kernel_entry, which jumps to main() in the kernel.
#	- the compiled C kernel.
# Note that --oformat binary deletes all gcc symbols.
kernel/kernel.bin: boot/kernel_entry.o ${OBJ}
	i386-elf-ld -o $@ -Ttext 0x1000 $^ --oformat binary

# Generic rule for compiling C code to an object file
# For simplicity, make C file depend on all header files.
%.o : %.c ${HEADERS}
	i386-elf-gcc ${CFLAGS} -ffreestanding -c $< -o $@

# Assemble the kernel_entry
%.o: %.asm
	nasm $< -f elf -o $@

# Rule to disassemble kernel. For debugging.
kernel.dis: kernel/kernel.bin
	ndisasm -b 32 $< > $@

######################
#   - Boot Sector    #
######################

# Generic rule for assembling asm to raw binary. 
# Use -I options to tell nasm where to find include files.
%.bin: %.asm
	nasm $< -f bin -o $@

######################
#   - Debug		  	 #
######################

# Create an object file with all symbols intact. 
# Flag -g enables debug symbols for GDB.
# Run: 'strings kernel.elf' to see the strings in the object file.
kernel/kernel.elf: boot/kernel_entry.o ${OBJ}
	i386-elf-ld -o $@ -Ttext 0x1000 $^

# Open the connection to qemu and load the kernel-object file with symbols
debug: os-image kernel/kernel.elf
	qemu-system-i386 -s -fda os-image &
	gdb -ex "target remote localhost:1234" -ex "symbol-file kernel/kernel.elf"

