#include "screen.h"
#include "ports.h"
#include "../kernel/util.h"

// Forward declaration of private functions

int GetCursorOffset();
void SetCursorOffset(int offset);
int PrintChar(char character, int col, int row, char attributeByte);
int Getoffset(int col, int row);
int GetOffsetRow(int offset);
int GetOffsetCol(int offset);


/**********************************************************
 * Public Kernel API functions                            *
 **********************************************************/

/**
 * Print a message on the specified location
 * If col, row, are negative, we will use the current offset.
 * TODO: 
 *	Function can be rewritten to only use offset, 
 *	and not recalculate col and row.
 * 		
 */
void PrintAt(char *message, int col, int row) 
{
	// Set cursor if col/row are negative
	int offset;
	if(col < 0 || row < 0)
	{
		offset = GetCursorOffset(col, row);
		row = GetOffsetRow(offset);
		col = GetOffsetCol(offset);
	}
	
    // Loop through message and print it
    int i = 0;
    while (message[i] != 0)
	{
        offset = PrintChar(message[i++], col, row, WHITE_ON_BLACK);

        // Compute row/col for next iteration.
        row = GetOffsetRow(offset);
        col = GetOffsetCol(offset);
    }
}

void PrintAtCursor(char *message)
{
	PrintAt(message, -1, -1);
}


/**********************************************************
 * Private kernel functions                               *
 **********************************************************/

/**
 * Prints a character at given position on screen, directly accesses the video memory.
 *
 * If 'col' and 'row' are negative, we will print at current cursor location
 * If 'attributeByte' is zero it will use 'white on black' as default
 * Returns the offset of the next character
 * Sets the video cursor to the returned offset
 */
int PrintChar(char character, int col, int row, char attributeByte)
{
	// Create a byte (char) pointer to the start of video memoy
	u8 *vidMemory = (u8 *) VIDEO_ADDRESS;

	// If attribute byte is zero, assmue default style
	if(!attributeByte)
	{
		attributeByte = WHITE_ON_BLACK;
	}

    /* Error control: print a red 'E' if the coords aren't right */
    if (col >= MAX_COLS || row >= MAX_ROWS) 
	{
        vidMemory[2*(MAX_COLS)*(MAX_ROWS)-2] = 'E';
        vidMemory[2*(MAX_COLS)*(MAX_ROWS)-1] = RED_ON_WHITE;
        return GetOffset(col, row);
    }

	// Get the video memory offset for the screen location.
	int offset;

	// If col and row are non-negative, use them for offset.
	if(col >= 0 && row >= 0)
	{	
		offset = GetOffset(col, row);
	}
	else // Otherwise, use current cursor
	{
		offset = GetCursorOffset();
	}

	// If we see a newline character, set offset to the end of
	// current row +1, so it will be advanced to the first col
	// of the next row.
	if(character == '\n')
	{
		int rows = GetOffsetRow(offset);
		offset = GetOffset(0, rows + 1);
	}
	// Otherwise, write the character and its attribute byte
	// to video memory at our calculcated offset.
	else	
	{
		vidMemory[offset] = character;
		vidMemory[offset+1] = attributeByte;
		offset += 2;
	}

	// Check if the offset is over screen size and scroll
    if (offset >= MAX_ROWS * MAX_COLS * 2)
	{
        int i;
        for (i = 1; i < MAX_ROWS; i++)
		{
            MemoryCopy((char*)GetOffset(0, i) + VIDEO_ADDRESS, 	/* Source: First line */
                       (char*)GetOffset(0, i-1) + VIDEO_ADDRESS, 	/* Destination: Second line */
                        MAX_COLS * 2);						/* Copy all columns */
		}

		//  Blank last line
        char *lastLine = (char*)((int)GetOffset(0, MAX_ROWS-1) + VIDEO_ADDRESS);
        for (i = 0; i < MAX_COLS * 2; i++) 
		{
			lastLine[i] = 0;
		}

		// Set offset from
		// last column AND last line to
		// the second to last line.
        offset -= 2 * MAX_COLS;
    }

	SetCursorOffset(offset);
	return offset;
}

int GetCursorOffset()
{
	// Use the VGA ports to get the current cursor position

	// Requesting byte 14: high byte of cursor position.
    port_byte_out(REG_SCREEN_CTRL, 14);
    
	// Data is returned in VGA data register.
	int offset = port_byte_in(REG_SCREEN_DATA) << 8; // High byte: << 8
    
	// Request byte 15: low byte of cursor position.
	port_byte_out(REG_SCREEN_CTRL, 15);
    
	// Data is returned in VGA data register
	offset += port_byte_in(REG_SCREEN_DATA);
    
	return offset * 2; // Position * size of character cell 
}

void SetCursorOffset(int offset)
{
	// Similar to getCursorOffset, but instead of reading, set data.

	// Remove multiple of 2 needed by video memory logic.
    offset /= 2;
	
	// Requesting byte 14: high byte of cursor position.
    port_byte_out(REG_SCREEN_CTRL, 14);

	// Write high byte of offset to VGA data register.
    port_byte_out(REG_SCREEN_DATA, (u8)(offset >> 8)); // Push top byte to bottom.

    // Request byte 15: low byte of cursor position.
    port_byte_out(REG_SCREEN_CTRL, 15);

	// Write low byte of offset to VGA data register.
    port_byte_out(REG_SCREEN_DATA, (u8)(offset & 0xff)); // Only keep bottom byte.
}

void ClearScreen()
{
	int screenSize = MAX_COLS * MAX_ROWS;
	int i;
	char *screen = (char*)VIDEO_ADDRESS;
	
	for(i = 0; i < screenSize; i++)
	{
		screen[i*2] = ' ';
		screen[i*2+1] = WHITE_ON_BLACK;
	}
	SetCursorOffset(GetOffset(0,0));
}

int GetOffset(int col, int row)
{
	return 2 * (row * MAX_COLS + col);
}

int GetOffsetRow(int offset)
{
	return offset / (2 * MAX_COLS); 
}

int GetOffsetCol(int offset) 
{ 
	return (offset - (GetOffsetRow(offset)*2*MAX_COLS))/2; 
}
