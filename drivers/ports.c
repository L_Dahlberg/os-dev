#include "ports.h"

// Recive data from 'port'
u8 port_byte_in(u16 port)
{
	// A C wrapper function that reads a byte from the specified port
	// 	"=a" (result) means: put al register in variable result when finished
	//  "d" (port) means: lead edx with port
	u8 result;
	__asm__("in %%dx, %%al" : "=a" (result) : "d" (port));
	return result;
}

// Send 'data' to I/O 'port'.
void port_byte_out(u16 port, u8 data)
{
	// "a" (data) means: load eax with data
	// "d" (port) means: load edx with port
	__asm__("out %%al, %%dx" : : "a" (data), "d" (port));
}

// Recive data from 'port'
u16 port_word_in(u16 port)
{
	u16 result;
	__asm__("in %%dx, %%ax" : "=a" (result) : "d" (port));
	return result;
}

// Send 'data' to I/O 'port'.
void port_word_out(u16 port, u16 data)
{
	__asm__("out %%ax, %%dx" : :"a" (data), "d" (port));
}
