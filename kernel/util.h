#ifndef UTIL_H
#define UTIL_H

#include "../cpu/types.h"

void MemoryCopy(char *source, char *dest, int nbytes);
void IntToAscii(int n, char str[]);

#endif
