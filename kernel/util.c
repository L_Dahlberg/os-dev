
/* 
 * Unsafely copy the nbytes pointed to by source to 
 * where dest is pointing at. 
 */
void MemoryCopy(char *source, char *dest, int nbytes)
{
    int i;
    for (i = 0; i < nbytes; i++)
 	{
        *(dest + i) = *(source + i);
    }
}

// Translates an integer to its ASCII value.
void IntToAscii(int n, char str[]) 
{
    int	sign = n;
	
	// Number is negative
    if (sign < 0)
	{
		 n = -n;
	}

	int i = 0;
    do 
	{
		// Offset from 0x30 (aka '0' in ascii)
		// if n < 10: Last digit, otherwise there is more.
		// if n == 8, str will be == 8 + '0' == 8 + 48 == 0x8 + 0x30 = 0x38 == '8'.
    	str[i++] = n % 10 + '0';
    }
	while ((n /= 10) > 0);

    if (sign < 0)
	{
		str[i++] = '-';
	}
    str[i] = '\0';

    /* TODO: implement "reverse" */
}
