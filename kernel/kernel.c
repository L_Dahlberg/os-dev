#include "../drivers/screen.h"
#include "util.h"
#include "../cpu/isr.h"
#include "../cpu/idt.h"

void main() 
{

    // Print testing
	ClearScreen();
	PrintAt("X", 1, 6);
    PrintAt("This text spans multiple lines", 75, 10);
    PrintAt("There is a line\nbreak", 0, 20);
    PrintAtCursor("There is a line\nbreak");
    PrintAt("What happens when we run out of space?", 45, 24);

	// Ideally pause the program.

    ClearScreen();

    /* Fill up the screen */
    int i = 0;
    for (i = 0; i < 24; i++) 
	{
        char str[255];
        IntToAscii(i, str);
        PrintAt(str, 0, i);
    }

    PrintAtCursor("\n\nThis text forces the kernel to scroll. Row 0 will disappear. ");
    PrintAtCursor("\nAnd with this text, the kernel will scroll again, and row 1 will disappear too!");

    ClearScreen();

    // Interrupts

    isr_install();
    // Test the interrupts
    __asm__ __volatile__("int $2");
    __asm__ __volatile__("int $3");
}

