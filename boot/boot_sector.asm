;
; A boot sector that boots a C kernel in 32-bit protected mode
;

[org 0x7c00]

KERNEL_OFFSET equ 0x1000            ; This is the memory offset to which we will load the kernel.
                                    ; Note that it is the same used when linking the kernel.

mov bp, 0x9000                      ; Set up the stack
mov sp, bp

mov bx, MSG_REAL_MODE               ; Announce that we are starting 
call print_string                   ; booting from 16-bit real mode.

call load_kernel                    ; Load our kernel from disk

call switch_to_pm                   ; Switch to PM, will not return

jmp $                               ; in case of error.

%include"boot/print/print_string.asm"
%include"boot/disk/loadDisk.asm"
%include"boot/GDT/flatGDT.asm"
%include"boot/print/print_string_pm.asm"
%include"boot/switch_to_pm.asm"

[bits 16]

load_kernel:
    mov bx, MSG_LOAD_KERNEL         ; Print a message
    call print_string
    
    mov bx, KERNEL_OFFSET           ; Set up parameters for disk_load, so that we load the first
    mov dh, 15                      ; 15 sectors (excluding the boot sector) from the boot disk.
    mov dl, [BOOT_DRIVE]            ; I.e. our kernel code is loaded to address KERNEL_OFFSET.
    call disk_load

    ret

[bits 32]
; This is where we arrive after switching to and initialising protected mode.

BEGIN_PM:
    mov ebx, MSG_PROT_MODE          ; Print a message using 32-bit print routine.
    call print_string_pm

    call KERNEL_OFFSET              ; Now jump to the address of our loaded kernel code.

    jmp $


; Global variables
BOOT_DRIVE db 0
MSG_REAL_MODE db "Started in 16 - bit Real Mode" , 0
MSG_PROT_MODE db "Successfully landed in 32 - bit Protected Mode" , 0
MSG_LOAD_KERNEL db "Loading kernel into memory." , 0

; Bootsector padding
times 510 - ($ - $$) db 0
dw 0xaa55



    
