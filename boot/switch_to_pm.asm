
[bits 16]

; Switch to protected mode
switch_to_pm:

 cli			        ; We must switch off interrupts until we have set up the protected mode (PM)
			            ; interrupt vector, otherwise system will not work well.

lgdt [gdt_descriptor]	; Load our global descriptor table, which defines our pm segments.

mov eax, cr0		    ; To make the switch to protected mode,
or eax, 0x1		        ; we set the first bit of cro (a control register)
mov cr0, eax

jmp CODE_SEG:init_pm	; Make a far jump (to a new segment) to our 32-bit code. 
			            ; This also forces the cpu to flush its cache of pre-fetched
			            ; and real-mode decoded instructions, which can cause problems.

[bits 32]

; Initialise registers and the stack once in PM.
init_pm:
 mov ax, DATA_SEG	    ; Now in PM, our old segments are meaningless, so we point our
 mov ds, ax		        ; segment registers to the data selector we defined in the GDT.
 mov ss, ax
 mov es, ax
 mov fs, ax
 mov gs, ax

 mov ebp, 0x90000	    ; Update our stack position so it is right at the top of the free space.
 mov esp, ebp

 call BEGIN_PM
