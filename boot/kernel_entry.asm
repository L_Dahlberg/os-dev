; Ensures that we jump straight into the kernel's entry function.

[bits 32]           ; In PM.
[extern main]       ; Declare that we will be referencing the external symbol called
                    ; 'main' so the linker can substitute it to the final address.

call main           ; Invoke main() in C kernel.
jmp $               ; Hang when we return from the kernel.

