; GDT - Global Descriptor Table.

gdt_start:

gdt_null:			            ; The Mandatory null descriptor (8 bytes)
 dd 0x0				            ; 'dd' means define double word (4 bytes in 16 bit mode)
 dd 0x0

gdt_code:			            ; The code segment descriptor (8 bytes)

 ; Base = 0x0, Limit 0xfffff
 ; 1st  flags: (Present)1     (Privilege)00     (Descriptor type)1 		-> 1001b
 ; Type flags: (Code)1	      (Conforming)0     (Readable)1        (Accessed)0	-> 1010b
 ; 2nd  flags: (Granularity)1 (32-bit default)1 (64-bit seg)0      (AVL)0	-> 1100b

 dw 0xffff			            ; Limit	(bits 0  - 15)
 dw 0x0 			            ; Base 	(bits 0  - 15)
 db 0x0				            ; Base 	(bits 16 - 23)
 db 10011010b			        ; 1st flags and Type flags.
 db 11001111b			        ; 2nd flags and Limit.
 db 0x0				            ; Base 	(bits 24 - 31)

gdt_data:			            ; The data segment descriptor (8 bytes)

 ; Same as code segment except for the type flags
 ; Type flags: (Code)0        (Expand down)0    (Writable)1        (Accessed)0  -> 0010b

 dw 0xffff              	    ; Limit (bits 0  - 15)
 dw 0x0                 	    ; Base  (bits 0  - 15)
 db 0x0                 	    ; Base  (bits 16 - 23)
 db 10010010b           	    ; 1st flags and Type flags.
 db 11001111b           	    ; 2nd flags and Limit.
 db 0x0                 	    ; Base  (bits 24 - 31)

gdt_end:			            ; The reason for this label is so we can calculate 
				                ; the size of the GDT for the GDT descriptor

; GDT descriptor
gdt_descriptor:
 dw gdt_end - gdt_start - 1	    ; Size of our GDT, always -1 of the true size.
 dd gdt_start			        ; Start address of our GDT.
 
; Define some constants for the GDT segment descriptor offsets
; These are what segment registers must contain when in protected mode.
; For example: When DS = 0x10 in PM (protected mode), the cpu knows what we mean it for 
; it to use the segment described at offset 0x10 (16 bytes) in our GDT,
; which in our case is the DATA segment (0x0 -> NULL; 0x08 -> CODE; 0x10 -> DATA)
CODE_SEG equ gdt_code - gdt_start
DATA_SEG equ gdt_data - gdt_start

