;
; function that loads dh sectors to ex:bs from drive dl
;

disk_load:
 push dx			    ; Store specifically dx on stack so we can remember
				        ; how many sectors were requested to be read.

 mov ah, 0x02			; BIOS read sector function
 mov al, dh			    ; Read dh sectors.
 mov ch, 0x00			; ch: Select CYLINDER 0 (inner most one).
 mov dh, 0x00			; dh: Select HEAD 0 (top disk?)
 mov cl, 0x02			; cl: Start reading from second sector (after the boot sector).
				        ; Sector size is 512 bytes (boot sector at 0x01?)
 int 0x13			    ; BIOS interrupt for reading from disk.

 jc disk_error 			; Jump if error (carry flag cf is set)
 
 pop dx 			    ; Restore dx from stack.
 cmp dh, al			    ; if al (number of sectors read) != dh (sector expected).
 jne disk_error			; display error message
 ret

disk_error:
 mov bx, DISK_ERROR_MSG
 call print_string 
 jmp $

;%include "boot/print/print_string.asm" 

; Variables
DISK_ERROR_MSG: 
 db "Disk read error!", 0


