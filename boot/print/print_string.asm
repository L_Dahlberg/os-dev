;
; Print function that make use out of the 0x10 interrupt. 
; Input string offset in bx, function stops reading at 0.
;

print_string:
    pusha
    mov ah, 0x0e
    START:
        mov al, [bx]
        cmp al, 0
        je END
        int 0x10
        add bx, 1
        jmp START
    END:
    call print_nl
    popa
    ret

print_nl:
    pusha
    mov ah, 0x0e
    mov al, 0x0a
    int 0x10
    mov al, 0x0d
    int 0x10
    popa
    ret

