[bits 32] 			            ; using 32-bit protected mode.

; Definition of constants
VIDEO_MEMORY equ 0xb8000	    ; The start of VGA video memory.
WHITE_ON_BLACK equ 0x0f 	    ; The color byte for each character.

; Prints a null-terminated string string pointed to by ebx (in parameter).
print_string_pm:
 pusha
 mov edx, VIDEO_MEMORY		    ; Set edx to the start of video memory.
 
 ;call print_string_pm_clear
 
print_string_pm_loop:
 mov al, [ebx]			        ; Store the char at ebx in al.
 mov ah, WHITE_ON_BLACK		    ; Store the attributes in ah.
 
 cmp al, 0			            ; if (al == 0), at the end of string or string empty.
 je print_string_pm_done	    ; If equal, jump to end.

 mov [edx], ax			        ; Store char and attributes at current character 
				                ; cell at video memory.
 add ebx, 1			            ; Increment ebx (input parameter) to the next character in string.
 add edx, 2			            ; Move to next character cell in video memory.
 
 jmp print_string_pm_loop   	; Loop around to print the next char

print_string_pm_done:
 popa
 ret

; print empty string to entire 80x25 grid (unused with kernel)
print_string_pm_clear:
 pusha
 mov eax, 0			            ; columns, 0 - 80
 mov ebx, 0			            ; rows, 0 - 25

print_string_pm_clear_loop:	    ; Reset variables
 mov edx, VIDEO_MEMORY
 mov ecx, 0

 add ecx, eax			        ; perform edx = VID_MEM + 2 * (ebx + eax).
 add ecx, ebx
 shl ecx, 1 
 add edx, ecx

 mov ecx, 0			            ; Print and reset ecx.
 mov cl, " "
 mov ch, WHITE_ON_BLACK
 mov [edx], cx

 add eax, 1			            ; Add 1 to eax and loop.
 cmp eax, 79			        ; When eax == 79, one row done
 jle print_string_pm_clear_loop
 add ebx, 80			        ; Move to the next row
 mov eax, 0			            ; Reset eax
 cmp ebx, 7999			        ; When ebx == ((80 * 25) -1 ) == 7999, all done
 jle print_string_pm_clear_loop
 
 popa 				            ; Real mode cursor still blinking
 ret

